<html>
<head>
    <script>
        function loads_data(){
            var hr = new XMLHttpRequest();
            var url = "page.php";
            var fn = document.getElementById("page").value;
            var vars = "page="+fn;
            hr.open("POST", url, true);
            hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            hr.onreadystatechange = function() {
                if(hr.readyState == 4 && hr.status == 200) {
                    var return_data = hr.responseText;
                    document.getElementById("status").innerHTML = return_data;
                }
            }
            hr.send(vars);
            document.getElementById("status").innerHTML = "processing...";
        }
    </script>
</head>
<body>
<div class="table-responsive" id="pagination_data"></div>
<div id="status"></div>
<h2>Ajax Post to PHP and Get Return Data</h2>
First Name: <input id="page" type="number"> <br><br>
<input name="myBtn" type="submit" value="Submit Data" onclick="loads_data();"> <br><br>

</body>
</html>